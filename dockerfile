# Useful link: https://github.com/google/aiyprojects-raspbian/blob/aiyprojects/HACKING.md

# Use the latest python docker image
FROM python:latest

# Create working directory
RUN mkdir /root/.ssh && mkdir /app

# Copy repository into working directory
ADD . /app/

# Set working directory for build context
WORKDIR /app

# Install python module pre-reqs
RUN pip install -r ./requirements.txt

# 
RUN echo "deb https://packages.cloud.google.com/apt aiyprojects-stable main" | tee /etc/apt/sources.list.d/aiyprojects.list && \
	curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
	apt-get update && apt-get upgrade && \
	apt-get install -y aiy-models

CMD python3