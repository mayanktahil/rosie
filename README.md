# Rosie

Rosie is the vision kit powered household device that will let you know if items you take pictures of can be recycled or not! Rosie uses google's image classification library to figure out what objects Rosie is looking at. Then, it tells you if that object is recyclable or not based on if we have defined it as recyclable or not. It can be used in just 4 simple steps: 
1. Plug Rosie into a computer
2. Point Rosie at the object you want to recycle
3. Rosie will tell you if that object is recyclable or not
4. Recycle or throw away the object. 
