#!/usr/bin/env python3
#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Camera image classification demo code.

Runs continuous image classification on camera frames and prints detected object
classes.

Example:
image_classification_camera.py --num_frames 10
"""
import argparse
import contextlib
import pickle
import pprint as pp
import time
import easygui

from aiy.vision.inference import CameraInference
from aiy.vision.models import image_classification
from picamera import PiCamera
from aiy.board import Board 
from aiy.leds import Leds, Color


def classes_info(classes):
    return ', '.join('%s (%.2f)' % pair for pair in classes)

def aggregate_score(classes, DB, counter):
    # If the confidence is lower than 0.42, don't do anything. Return the results back with no change.
    if classes[0][1] < 0.42: 
        return

    # Some objects are classified together with multiple names but each name in the list is delimited by '/'      
    items=classes[0][0].split('/')

    # If the object classified has only 1 name, then just save as an array of length 1 with the name
    if not isinstance([0, 10, 20, 30], list):
        items = []
        items[0] = classes[0][0].split('/')
    # For each item name
    for item in items:
        # Check if the name is in the database
        if item in DB.inventory:
            # At this point, the item name is in the database with a confidence level greater than 0.6
            # Save the item in results to count how many times it occurs
            counter.add(item)

@contextlib.contextmanager
def CameraPreview(camera, enabled):
    if enabled:
        camera.start_preview()
    try:
        yield
    finally:
        if enabled:
            camera.stop_preview()

class results():
    def __init__(self):
        self.counter = {}

    def add(self,item):
        if item in self.counter:
            self.counter[item] += 1
        else:
            self.counter[item] = 1

    def reset(self):
        self.counter = {}

    def display(self):
        pp.pprint(self.counter)

    def answer(self, DB):
        for item in self.counter:
            if self.counter[item] > 3:
                return item, DB.inventory[item]
        return "Unknown Item", "Sorry, not sure how to recycle this item."

class database():
    """Class object that stores all the items we will check against to recycle"""
    def __init__(self):
        self.inventory = {}

    # Function to add an item to our DB collection. Not 
    def add(self, item, details):
        self.inventory[item] = details

    # Function to remove an item from our DB collection
    def delete(self, item):
        del self.inventory[item]

    # Function to find information from our DB collection for a particular item
    def find(self, name):
        return self.inventory[item]

    def print(self):
        pp.pprint(self.inventory)

    # Function to save our initial DB collection
    def export(self, data):
        pickle.dump(data, open( "DB.pickle", "wb" ))

def main():

# define arguments and definitions
    parser = argparse.ArgumentParser('Image classification camera inference example.')
    parser.add_argument('--num_frames', '-n', type=int, default=10,
        help='Sets the number of frames to run for, otherwise runs 10.')
    parser.add_argument('--num_objects', '-c', type=int, default=1,
        help='Sets the number of object interences to print.')
    parser.add_argument('--nopreview', dest='preview', action='store_false', default=False,
        help='Enable camera preview')
    parser.add_argument('--picklefile', '-pf', default='./db.pickle',
        help='Pickled file of python dictionary object in {Item String : Description String} format to load as DB')
    args = parser.parse_args()

    # Load DB into memory to cross reference
    DB = pickle.load(open('DB.pickle', 'rb'))
    counter = results()
    leds = Leds()
    # Uncomment next line for Debugging -- Prints the database into the terminal
    # DB.print()

    # Load to see if button is pressed on the board
    with Board() as board:

        # Keep alive while loop
        while True:
            # Turn on LED to show it's alive
            leds.update(Leds.rgb_on(Color.RED))
            # Pause logic until button is pressed
            board.button.wait_for_press()
            # Turn Off LED to indicate it's capturing frames
            leds.update(Leds.rgb_on(Color.BLUE))
            # Engage camera for picture frames
            with PiCamera(sensor_mode=4, framerate=30) as camera, \
                 CameraPreview(camera, enabled=args.preview), \
                 CameraInference(image_classification.model()) as inference:
                for result in inference.run(args.num_frames):
                    classes = image_classification.get_classes(result, top_k=args.num_objects)
                    print(classes_info(classes))
                    if classes:
                        camera.annotate_text = '%s (%.2f)' % classes[0]

                    # Determine if item detected is Database. 
                    aggregate_score(classes, DB, counter)
            
            # Reset counter for next round of identifications
            [item, details] = counter.answer(DB)
            prompt = "%s : %s" % (item, details)
            pp.pprint(prompt)

            # Try to prompt GUI message box for visual prompt, if not display image that we cannot prompt message
            try:
                easygui.msgbox(prompt, 'Recycling Details')
            except:
                pp.pprint("Display prompt is not available. Is the display connected?")

            leds.update(Leds.rgb_on(Color.GREEN))
            counter.reset()
            time.sleep(1)

if __name__ == '__main__':
    main()
