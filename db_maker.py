#!/usr/bin/env python3

import pickle
from image_classification_camera import database

def main():
    DB = database()
    
    # Cardboard 
    # Source https://earth911.com/recycling-guide/how-to-recycle-cardboard/
    DB.add(item='cardboard', details='The first step in recycling cardboard is to separate it from other paper, such as office paper and newspaper. You can recycle cardboard through any local recycling collection services. Just place in the appropriate bin near you.')
    
    # Plastic Cup
    # Source https://www.keeptruckeegreen.org/guide/plastic-cups/
    DB.add(item='plastic cup', details='You can send in any rigid, plastic #6 cup to the Solo Cup Brigade, which is a free recycling program through TerraCycle.')

    # Plastic bottle
    # Source https://earth911.com/recycling-guide/how-to-recycle-plastic-jugs-bottles/
    DB.add(item='water bottle', details='Most bottles and jugs are #1 plastic (PET) or #2 plastic (HDPE), which are both accepted by most curbside recycling programs. The type of plastic is identified with a resin ID code on the bottle.')
    DB.add(item='pop bottle', details='Most bottles and jugs are #1 plastic (PET) or #2 plastic (HDPE), which are both accepted by most curbside recycling programs. The type of plastic is identified with a resin ID code on the bottle.')
    DB.add(item='soda bottle', details='Most bottles and jugs are #1 plastic (PET) or #2 plastic (HDPE), which are both accepted by most curbside recycling programs. The type of plastic is identified with a resin ID code on the bottle.')

    # Coffee Mug
    # Source https://keeptruckeegreen.org/dishes-are-not-recyclable/
    DB.add(item='coffee mug', details='If dishes are broken, or have bad chips, cracks or stains, toss them. Wrap any sharp edges or pieces in newspaper, place them in a plastic bag, label them as “broken glass,” and throw them away. Broken glass is never recyclable because it is a hazard for sanitation workers to handle it.')

    DB.export(data=DB)

if __name__ == '__main__':
    main()
